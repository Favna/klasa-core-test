import { Client, ClientEvents } from '@klasa/core';
import 'module-alias/register';
import { token } from './config';

const client = new Client();

client.on(ClientEvents.Ready, () => {
  client.emit(ClientEvents.Debug, 'Shard Ready');
});

client.on(ClientEvents.Debug, (msg) => {
  console.log(msg);
});

client.on(
  ClientEvents.MessageCreate,
  async (msg): Promise<void> => {
    if (msg.author.bot) return;
    if (msg.content.toLowerCase().startsWith('!ping')) {
      const [response] = await msg.channel.send((mb) => mb.setContent('ping?'));
      await response.edit((mb) => mb.setContent(`Pong! Took: ${response.createdTimestamp - msg.createdTimestamp}ms`));
    }
  }
);

client.on('messageCreate', async (msg) => {
  if (msg.author.bot) return;
  if (msg.content.toLowerCase().startsWith('Open the pod bay doors HAL')) {
    await msg.channel.send((mb) => mb.setContent("I'm sorry, Dave. I'm afraid I can't do that."));
  }
});

client.token = token;

client.connect().catch(() => undefined);
